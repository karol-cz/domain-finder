#!/bin/sh

BASE_DIR=$(dirname "$0")
BUILD_DIR=${BASE_DIR}/build
SOURCE_DIR=${BASE_DIR}/src

mkdir -p $BUILD_DIR

javac -d $BUILD_DIR $SOURCE_DIR/DomainFinder.java

java -classpath $BUILD_DIR DomainFinder $1
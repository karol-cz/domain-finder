import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DomainFinder {
	
	private static final String A_HREF_REGEX = "<a.+?href=\"http[s]?:\\/\\/(.+?)(?:\\/.+?)?\".*?>";
	private static final Pattern A_HREF_PATTERN = Pattern.compile(A_HREF_REGEX, Pattern.DOTALL);
	
	public static void main(String[] args) throws IOException {
		if(args.length == 1) 
			inspectUrl(args[0]);
		else
			inspectUrl("https://www.orange.pl/");
	}
	
	private static void inspectUrl(String input) {
		try {
			printResults(findDomains(input));
		}
		catch (MalformedURLException e) {
			System.out.println("Invalid URL: "+input);
		}
		catch (IOException e) {
			System.out.println("Error retrieving website content: "+e.getMessage());
		}
	}

	private static void printResults(Map<String, Integer> map) {
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
	}
	
	private static  Map<String, Integer> findDomains(String input) throws IOException {
		URL url = new URL(input);
		
		try (InputStream inputStream = url.openStream()){
			return findDomains(inputStream);
		} 
	}
	
	private static Map<String, Integer> findDomains(InputStream is) throws IOException {
		return extractDomains(toString(is));
	}
	
	private static Map<String, Integer> extractDomains(String content) {
		Map<String, Integer> results = new TreeMap<>();
		
		Matcher matcher = A_HREF_PATTERN.matcher(content);
		while (matcher.find()) {
			incrementResults(results, matcher.group(1));
		}
		return results;
	}

	private static void incrementResults(Map<String, Integer> results, String input) {
		if (results.containsKey(input)) {
			results.put(input, results.get(input) + 1);
		} else {
			results.put(input, 1);
		}
	}
	
	private static String toString(InputStream is) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
		StringBuilder sb = new StringBuilder();
		String inputLine;
		while((inputLine = in.readLine()) != null) {
			sb.append(inputLine).append("\n");
		}
		return sb.toString();
	}
}

## Domain Finder

This simple application finds all domains referenced from a given URL.

#### Usage:
./search.sh [URL]